from django.db import models


# Create your models here.
class Gallery(models.Model):
    title = models.TextField()
    cover = models.ImageField(upload_to='images/')
    objects = models.Manager()

    def __str__(self):
        return self.title


class BillReceipt(models.Model):
    date = models.DateField()
    name = models.CharField(max_length=150)
    address = models.CharField(max_length=150)
    amount_recieved_by_trust_in_rupees = models.CharField(max_length=50)
    payment_mode = models.CharField(max_length=50)

    class Meta:
        db_table = 'bill_receipt'
        managed = True

    def __str__(self):
        return self.name

