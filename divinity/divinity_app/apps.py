from django.apps import AppConfig


class DivinityAppConfig(AppConfig):
    name = 'divinity_app'
