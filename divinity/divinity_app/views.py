from datetime import datetime
from io import BytesIO
from django.template import loader
from django.http import HttpResponse
from django.template.loader import get_template
from .models import Gallery
from xhtml2pdf import pisa


# Create your views here.
def index(request):
    # getting our template
    template = loader.get_template('index.html')

    # rendering the template in HttpResponse
    return HttpResponse(template.render())


def about(request):
    # getting our template
    template = loader.get_template('about.html')

    # rendering the template in HttpResponse
    return HttpResponse(template.render())


def contact(request):
    # getting our template
    template = loader.get_template('contact.html')

    # rendering the template in HttpResponse
    return HttpResponse(template.render())


def render_to_pdf(path: str, params: dict):
    template = get_template(path)
    html = template.render(params)
    response = BytesIO()
    pdf = pisa.pisaDocument(BytesIO(html.encode("UTF-8")), response)
    if not pdf.err:
        return HttpResponse(response.getvalue(), content_type='application/pdf')
    else:
        return HttpResponse("Error Rendering PDF", status=400)


def pdf_download(request):
    data = {
         'today': datetime.now(),
         'amount': 39.99,
         'customer_name': 'Cooper Mann',
         'order_id': 1233434,
    }
    pdf = render_to_pdf('invoice.html', data)
    return HttpResponse(pdf, content_type='application/pdf')


def gallery(request):
    # getting our template
    template = loader.get_template('gallery.html')
    context = {'gallery': Gallery.objects.all()}
    # rendering the template in HttpResponse
    return HttpResponse(template.render(context=context))


def seva(request):
    # getting our template
    template = loader.get_template('seva_and_pujas.html')
    # rendering the template in HttpResponse
    return HttpResponse(template.render())


def stothra(request):
    # getting our template
    template = loader.get_template('stothra.html')
    # rendering the template in HttpResponse
    return HttpResponse(template.render())