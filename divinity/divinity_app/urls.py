from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('about', views.about, name='about'),
    path('contact', views.contact, name='contact'),
    path('seva', views.seva, name='seva'),
    path('stothra', views.stothra, name='stothra'),
    path('gallery', views.gallery, name='gallery'),
    path('pdf_download', views.pdf_download, name='pdf_download'),
]